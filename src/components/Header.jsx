import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import { withStyles } from '@material-ui/core/styles';

import logoSrc from './qotient.svg';

const Header = ({ classes }) => {
  return (
    <AppBar className={classes.root} position="sticky">
      <Toolbar>
        <img width={120} src={logoSrc} />
      </Toolbar>
    </AppBar>
  );
};

const styles = (theme) => ({
  root: {
    background: 'white',

    transition: theme.transitions.create('box-shadow', { duration: theme.transitions.complex }),
    zIndex: 'auto',
  },
  title: {
    paddingLeft: theme.spacing(3),
    fontWeight: theme.typography.fontWeightRegular,
  }
});

export default withStyles(styles)(Header);
