import React from 'react';
import { gql } from 'apollo-boost';
import { Mutation } from 'react-apollo';

const ADD_STAR_MUTATION = gql`
  mutation ($input: AddStarInput!) {
    addStar(input: $input) {
      starrable {
        id
        viewerHasStarred
      }
    }
  }
`;

// This button only Stars a repository, but cannot unstar...
// A remove star mutation needs to be added
const toggleStarButton = ({ viewerHasStarred, id }) => (
  <Mutation
    mutation={ADD_STAR_MUTATION}
  >
    { (toggleStar) =>
      <button onClick={() =>
        toggleStar({
          variables: {
            input: {
              starrableId: id
            }
          }
        })
      }>{ viewerHasStarred ? 'Unstar' : 'Star' }</button>
    }
  </Mutation>
);

export default toggleStarButton;