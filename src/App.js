import React from 'react';

import { gql } from 'apollo-boost';
import { Query } from 'react-apollo';

import './App.css';

import Container from '@material-ui/core/Container';
import Header from './components/Header';

const SEARCH_REPO_QUERY = gql`
  query searchRepo {
    search(query: "react", type: REPOSITORY, first: 10) {
      nodes {
        ... on Repository {
          id
          name
          description
          stargazers {
            totalCount
          }
          viewerHasStarred
          updatedAt
        }
      }
      repositoryCount
      pageInfo {
        hasNextPage
        endCursor
      }
    }
  }
`;

function App() {
  return (
    <div className="App">
      <Header />
      <Container maxWidth="lg">
        <Query
          query={SEARCH_REPO_QUERY}
          // variables={{
          // TODO: pass queryString here
          // }}
        >
          { ({ loading, data }) => {
            // loading will be true during fetching of data.
            // data contains the response data
            console.log(data)
            // TODO: Render data in a table
            return (
              <div>Render data as table here</div>
            )
          }}
        </Query>
      </Container>
    </div>
  );
}

export default App;
