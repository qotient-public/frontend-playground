# Qotient Front End Playground

Congratulations on making through the first interview. Now this is time to show off your skils.
This repository is a webapp template forted from create-react-app and pre-installed few packages the Qotient platform uses.

## Getting Started

Make sure port 3000 is available prior to execute the commands.

```
yarn install
yarn start
```
 
### Prerequisites

Incase of failure, please check your node, npm and yarn version.
yarn -> 1.15.2
node -> 12.7.0
npm -> 6.10.0


### Package Links
Please refer to these websites for source of documentation:
Material-UI: http://material-ui.com
React Apollo, GraphQL: https://www.apollographql.com/docs/react/api/react-apollo/
Lodash: https://lodash.com/docs/4.17.15


## Extra ##
If you have any issue, please reach out to danny.cho@qotient.com or yaohong.chun@qotient.com immediately. Good luck!

